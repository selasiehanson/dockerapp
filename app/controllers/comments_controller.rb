class CommentsController < ApplicationController

  def create
    @article = Article.find params[:article_id]
    @comment = @article.comments.build comments_params

    if @comment.save
      redirect_to @article, notice: "Thanks for posting the comment"
    else
      redirect_to @article, aler: "Something went wrong"
    end
  end

  def comments_params
    params.require(:comments).permit(:name, :email, :body)
  end
end
