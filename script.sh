export APP_PWD=password
#create a docker container to hold the postgres database engine and link to another container that holds the actual data
#docker run -v /var/lib/p_stgresql/data --name app_data busybox
#removing the app_data container will make u loose all the data

db(){
  docker run -P --volumes-from app_data --name app_db -e POSTGRES_USER=app_user -e POSTGRES_PASSWORD=$APP_PWD -d -t postgres:latest
}


#we must build our image from our Docker file with tag selasiehanson/app 
#run docker build -t selasiehanson/app . from the cli

#app creates a container from our image we just built
app() {
  docker stop app
  docker rm app
  docker run -p 80:80 --link app_db:postgres --name app selasiehanson/app 
}


action=$1

${action} #sh script.sh db
